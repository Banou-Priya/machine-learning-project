# Machine learning project

## Auteurs :
 - Djamila HOCINE
 - Hamza JERANDO
 - Omar ERRIAHI EL IDRISSI
 - Banou-Priya SINNATAMBY

## Ce projet contient : 
 - le fichier README
 - le repertoire project
 - le fichier TunningDesHyperParamètres.xlsx qui contient le tableau pour la question des tests de performances 

Le code du projet se trouve dans le fichier project/track2vec.ipynb



